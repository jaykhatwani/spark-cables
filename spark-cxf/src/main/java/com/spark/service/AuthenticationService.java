package com.spark.service;

import java.util.Date;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spark.domain.Admin;




@Service
@Path("/authenticate")
public class AuthenticationService {

	@Autowired
	private SessionFactory sessionFactory;
	private Long TIME_OUT = 30000L;
	public  SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public  void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	
	@POST
	@Produces("text/plain")
	@Path("/validateLogin")
	public String validateLogin(
	        @FormParam("userId") String userId,
	        @FormParam("password") String password){
		
		
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		Admin admin = (Admin) session.get(Admin.class, userId);
		Date currDate = new Date();
		//verify if the token sent by UI is already in the DB
		if(admin.getToken().equals(password) && ( currDate.getTime() - admin.getLastUpdateDate().getTime()) < TIME_OUT ){
			return password;
		}
		
		
		
		//new token has been sent.. first time login or the session has expired.
		String dbpassword = admin.getPassword();
		//encrypt the password from DB with the same method used in FE to verify the password
		String dbpassword_token = encryptPassword(dbpassword);
		
		
		if(dbpassword_token.equals(password)){
			admin.setToken(dbpassword_token);
			admin.setLastUpdateDate(new Date());
			session.save(admin);
		}
		else{
			return "login_fail";
		}
		session.getTransaction().commit();
		session.close();
		return dbpassword_token;
	}
	
	private String encryptPassword(String password){
		
		return password;
	}
	
}
