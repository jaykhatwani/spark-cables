package com.spark.service;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spark.domain.Customer;
import com.spark.domain.CustomerBillGenerationDetails;



@Service
@Path("/customer")
public class CustomerService {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private AuthenticationService authService;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	//update in existing customer
	@POST
	@Produces("text/plain")
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/saveCustomer")
	public Response saveCustomer(@HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user,Customer cust
	      ){
		
		if(user==null || token ==null || !authenticateUser(user,token)) return Response.status(Response.Status.UNAUTHORIZED).build();
		Session session = getSessionFactory().openSession();
		
		session.beginTransaction();
		session.update(cust);
		session.getTransaction().commit();
		session.close();
		return Response.status(Response.Status.OK).build();
	}
	
	
	//add a new customer
	@POST
	@Produces("text/plain")
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/addCustomer")
	public Response addCustomer(@HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user,Customer cust
	      ){
		if(user==null || token ==null || !authenticateUser(user,token)) return Response.status(Response.Status.UNAUTHORIZED).build();
		Session session = getSessionFactory().openSession();
		
		session.beginTransaction();
		session.save(cust);
		session.getTransaction().commit();
		session.close();
		return Response.status(Response.Status.OK).build();
	}

	
	//generate bill of a particular customer
	@POST
	@Produces("text/plain")
	@Path("/custgenerateBill")
	public Response generateBill(  @HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user,
			@FormParam("amount") int amount,
			@FormParam("date") Date date,
	        @FormParam("customerId") Long customerId){
		
		if(user==null || token ==null || !authenticateUser(user,token)) return Response.status(Response.Status.UNAUTHORIZED).build();
		Session session = getSessionFactory().openSession();
		
		session.beginTransaction();
		CustomerBillGenerationDetails custbillDetails = new CustomerBillGenerationDetails();
		
		custbillDetails.setDate(date);
		custbillDetails.setBillAmount(amount);
		Customer cust = (Customer) session.get(Customer.class, customerId);
		int balance = cust.getBalance()+amount;
		custbillDetails.setCust(cust);
		cust.setBalance(balance);
		session.save(cust);
		session.save(custbillDetails);
		session.getTransaction().commit();
		session.close();
		return Response.status(Response.Status.OK).build();
	}
	

	//get bill generation data of a particular customer
	@GET
	@Consumes({"application/xml","application/json","application/x-www-form-urlencoded"})
	@Produces({"application/xml","application/json"})
	@Path("/displayCustBillGeneration")
	public Response getCustBillGeneration(@HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user, @FormParam("customerId") String customerId)
	        		{
		if(user==null || token ==null || !authenticateUser(user,token)) return Response.status(Response.Status.UNAUTHORIZED).build();
		Session session = getSessionFactory().openSession();
			
		session.beginTransaction();
		Query query = session.createQuery("from CustomerBillGenerationDetails where cust_customerId = :customerId");
		query.setParameter("customerId", customerId);
		query.setMaxResults(10);
		List<Customer> list = query.list();
		session.close();
		return Response.ok(list).type("application/json").build();
		}
	
	
	//search customer with name for autocomplete
	@GET
	@Consumes({"application/xml","application/json","application/x-www-form-urlencoded"})
	@Produces({"application/xml","application/json"})
	@Path("/searchCustomer")
	public Response searchCustomer(@HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user,
	        @FormParam("name") String name)
	        		{
		
		Customer customer = null;
		if(user==null || token ==null || !authenticateUser(user,token)) return Response.status(Response.Status.UNAUTHORIZED).build();
		Session session = getSessionFactory().openSession();
		
		session.beginTransaction();
		Query query = session.createQuery("from Customer where customerName like :name ");
		query.setParameter("name", "%"+name+"%");
		query.setMaxResults(10);
		List<Customer> list = query.list();
		
		session.close();
		return Response.ok(list).type("application/json").build();
		}
	
	
	private boolean authenticateUser(String user, String token) {
		
		if(authService.validateLogin(user, token).equals("login_fail")){
			return false;
		}
		else {
			return true;
		}
	//	return true;
	}

//search customer by name 
	@GET
	@Consumes({"application/xml","application/json","application/x-www-form-urlencoded"})
	@Produces({"application/xml","application/json"})
	@Path("/searchCustomerName")
	public Response searchCustomerName(@HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user,
	        @FormParam("name") String name)
	        		{
		if(user==null || token ==null || !authenticateUser(user,token)) return Response.status(Response.Status.UNAUTHORIZED).build();
		Session session = getSessionFactory().openSession();
			
		session.beginTransaction();
		Query query = session.createQuery("from Customer where customerName = :name ");
		query.setParameter("name", name);
		query.setMaxResults(10);
		List<Customer> list = query.list();

		session.close();
		return Response.ok(list).type("application/json").build();
		}
	
/*Methods to retrive customer by phone number begins*/
	
//search customer by phone for autocomplete
	@GET
	@Consumes({"application/xml","application/json","application/x-www-form-urlencoded"})
	@Produces({"application/xml","application/json"})
	@Path("/searchCustomerPhoneAutoComplete")
	public Response searchCustomerPhoneAutoComplete(@HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user,
	        @FormParam("phone") String phone)
	        		{
		Customer customer = null;
		if(user==null || token ==null || !authenticateUser(user,token)) return Response.status(Response.Status.UNAUTHORIZED).build();
		Session session = getSessionFactory().openSession();
			
		session.beginTransaction();
		System.out.println("phone is   "+phone);
		Query query = session.createQuery("from Customer where customerPhone like :phone ");
		query.setParameter("phone", "%"+phone+"%");
		query.setMaxResults(10);
		List<Customer> list = query.list();
		
		session.close();
		return Response.ok(list).type("application/json").build();
}
	
	//search customer by phone
	@GET
	@Consumes({"application/xml","application/json","application/x-www-form-urlencoded"})
	@Produces({"application/xml","application/json"})
	@Path("/searchCustomerPhone")
	public Response searchCustomerPhone(@HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user,
	        @FormParam("phone") String phone)
	        		{
		if(user==null || token ==null || !authenticateUser(user,token)) return Response.status(Response.Status.UNAUTHORIZED).build();
		Session session = getSessionFactory().openSession();
		
		session.beginTransaction();
		Query query = session.createQuery("from Customer where customerPhone = :phone ");
		query.setParameter("phone", phone);
		//query.setMaxResults(10);
		List<Customer> list = query.list();
		
		
		session.close();
		return Response.ok(list).type("application/json").build();
	}

	
	//Methods to retrieve customer by Phone number ends..
	

}
