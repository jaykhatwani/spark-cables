package com.spark.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spark.domain.BillGenerationDetails;
import com.spark.domain.Customer;
import com.spark.domain.CustomerBillGenerationDetails;



@Service
@Path("/bill")
public class BillService {
	
	@Autowired
    private  SessionFactory sessionFactory;
	@Autowired
	private AuthenticationService authService;
	
	@POST
	@Produces("text/plain")
	@Path("/generateBillAll")
	@Consumes(MediaType.APPLICATION_JSON)
	public String generateBillAll( @HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user, BillGenerationDetails billGenerationDetails){
		
		int amount = billGenerationDetails.getAmount();
		Date date = billGenerationDetails.getDate();
		
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		//to save generic bill generation details.
		session.save(billGenerationDetails);
		
		// to save customer-specific records
		Query query = session.createQuery("from Customer where active = :active ");
		query.setParameter("active", true);
		List<Customer> list = query.list();
		Customer customer = null;
		CustomerBillGenerationDetails customerBillGenerationDetails = new CustomerBillGenerationDetails();
		
		Iterator<Customer> itr=list.iterator();  
	    while(itr.hasNext()){  
	    	customer=itr.next(); 
	    	customerBillGenerationDetails.setCust(customer);
	    	customerBillGenerationDetails.setBillAmount(amount);
	    	customerBillGenerationDetails.setDate(date);
	    	session.save(customerBillGenerationDetails);
	    	session.evict(customerBillGenerationDetails);
	}  

	    
		//to update customer balance
		query = session.createQuery("update Customer set balance = balance + :amount where active = :active ");
		query.setParameter("amount", amount);
		query.setParameter("active", true);
	    query.executeUpdate();
	
		session.getTransaction().commit();
		session.close();
		//sessionFactory.close();
		return "success";
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@GET
	@Consumes({"application/xml","application/json","application/x-www-form-urlencoded"})
	@Produces({"application/xml","application/json"})
	@Path("/displayBillGeneration")
	public Response getBillGeneration( @HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user)
	        		{
		
		
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		Query query = session.createQuery("from BillGenerationDetails");
		query.setMaxResults(20);
		List<Customer> list = query.list();
		session.close();
		//sessionFactory.close();
		if(list==null){
		return Response.status(Response.Status.NO_CONTENT).build();	
		}else{
		return Response.ok(list).type("application/json").build();
	}}
	
	
	
	
private boolean authenticateUser(String user, String token) {
		
		if(authService.validateLogin(user, token).equals("login_fail")){
			return false;
		}
		else {
			return true;
		}
	//	return true;
	}

	

}
