package com.spark.service;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spark.domain.Customer;
import com.spark.domain.PaymentDetails;



@Service
@Path("/payment")
public class PaymentService {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private AuthenticationService authService;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@POST
	@Produces("text/plain")
	@Path("/makePayment")
	public int makePayment( @HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user,
	        @FormParam("payment") int payment,
	        @FormParam("customerId") Long customerId){
		
		
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		PaymentDetails paymentDetails = new PaymentDetails();
		
		paymentDetails.setDate(new Date());
		paymentDetails.setPayment(payment);
		Customer cust = (Customer) session.get(Customer.class, customerId);
		int balance = cust.getBalance()-payment;
		paymentDetails.setCust(cust);
		cust.setBalance(balance);
		session.save(cust);
		session.save(paymentDetails);
		session.getTransaction().commit();
		session.close();
		return balance;
	}
	
	
	
	@GET
	@Consumes({"application/xml","application/json","application/x-www-form-urlencoded"})
	@Produces({"application/xml","application/json"})
	@Path("/getPaymentDetails")
	public Response getPaymentDetails( @HeaderParam("Authorization") String token,
			@HeaderParam("spark-user") String user,
	        @FormParam("customerId") String customerId)
	        		{
		
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		Query query = session.createQuery("from PaymentDetails where cust_customerId = :customerId ");
		query.setParameter("customerId", customerId);
		query.setMaxResults(10);
		List<Customer> list = query.list();
		
			session.close();
	
		if(list==null){
		return Response.status(Response.Status.NO_CONTENT).build();	
		}else{
			System.out.println(Response.ok(list).build());
		return Response.ok(list).type("application/json").build();
	}}

	
	
private boolean authenticateUser(String user, String token) {
		
		if(authService.validateLogin(user, token).equals("login_fail")){
			return false;
		}
		else {
			return true;
		}
	//	return true;
	}

	

}
