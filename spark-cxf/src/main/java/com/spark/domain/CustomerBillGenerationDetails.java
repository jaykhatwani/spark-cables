package com.spark.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class CustomerBillGenerationDetails {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@XmlElement
	private Long CustBillId;
	@ManyToOne
	private Customer cust;
	private Date date;
	private int billAmount;
	
	
	public Long getCustBillId() {
		return CustBillId;
	}
	public void setCustBillId(Long custBillId) {
		CustBillId = custBillId;
	}
	public Customer getCust() {
		return cust;
	}
	public void setCust(Customer cust) {
		this.cust = cust;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getBillAmount() {
		return billAmount;
	}
	public void setBillAmount(int billAmount) {
		this.billAmount = billAmount;
	}
	
}
