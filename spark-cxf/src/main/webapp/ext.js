checkAuthentication();
	function checkAuthentication(){
  		
  		var sparkUser = $.cookie("sparkUser");
  		var token = $.cookie("sparkToken");
  		
  		if(token == null || sparkUser == null){
  			window.location.href= "index.jsp";
  		}else
  		authenticate(sparkUser,token,token);
  	};
  	

$(document).ready(function(){
	//checkAuthentication();
	$('#save').hide();
	var user =$.cookie("sparkUser");
	var token=$.cookie("sparkToken");
	$('#accordion').accordion({heightStyle: "content",
            autoHeight: false,
        clearStyle: true,   
        });
	  	$( '#main1' ).tabs();
	  	$( "#datepicker" ).datepicker();
	  	$( "#custdatepicker" ).datepicker();
	    
	  	$("#accordion :input").keyup(function(){
	  	//alert("changed");
	  		$("#save").show().animate({left:"0"},1000);
	  	}
	  	);

	  	displayBillGenerationData();
	  	
	  	 $('#generateBill').click(function(){
	  		
	  		var amount=$('#billAmount').val();
	  		var date = $('#datepicker').val();
	  		if(amount != undefined && amount != ''){
	  		 $.ajax({		  
	  			  url: "/spark-cxf/rest/bill/generateBillAll",
	  			  type: "POST",
	  			 contentType: 'application/json; charset=UTF-8',
	  			 headers: { 'x-spark-auth-token': token, 'x-spark-username' : user },
	  			  data: JSON.stringify({ amount: amount, date : new Date(date)}),
	  			  success: function(data){
	  				displayBillGenerationData();
	  			              },
	  			  error: function(data){
	  				  alert("An error occured while making the payment. Please try again");
	  			  }
	  			});
	  			 
	  		}
	  	 });
	  	         
	  	 function displayBillGeneration(){
	  		 $("#custbillgeneration tr").remove();
	  		$('#custbillgeneration').dataTable().fnDestroy();
	  		 var customerId = $("#customerId").val();
	  		  $.ajax({
	  			  url: "/spark-cxf/rest/customer/displayCustBillGeneration",
	  			  type: "GET",
	  			  datatype:"json",
	  			 headers: { 'Authorization': token, 'spark-user' : user },
	  			  contentType:"application/x-www-form-urlencoded",
	  			  accepts: {
	  			        text: "application/json"
	  			    },
	  			    data:{customerId:customerId},
	  			  success: function(data){
	  				  if(data){
	  	              var len = data.length;
	  	              var txt = "";
	  	              if(len > 0){
	  	            	  txt += "<thead><th>"+"Date" +"</th><th>" + "Amount" +"</th></thead>";
	                        
	  	                  for(var i=0;i<len;i++){
	  	                      if(data[i].billAmount && data[i].date){
	  	                    	  txt += "<tr><td>"+$.datepicker.formatDate('MM dd, yy', new Date(data[i].date))+ "</td><td>"+data[i].billAmount+"</td></tr>";
	  	                      }
	  	                  }
	  	                  if(txt != ""){
	  	                      $("#custbillgeneration").append(txt);
	  	                    
	  	                	$('#custbillgeneration').dataTable({});
	  	                  }
	  	              }
	  	          }
	  				   }
	  		  });
	  		 
	  	  }
	  	  

	
	  	 $('#custgenerateBill').click(function(){
		  		
		  		var amount=$('#custbillAmount').val();
		  		var date = $('#custdatepicker').val();
		  		var customerId = $('#customerId').val();
		  		if(amount != undefined && amount != ''){
		  		 $.ajax({		  
		  			  url: "/spark-cxf/rest/customer/custgenerateBill",
		  			  type: "POST",
		  			 contentType:"application/x-www-form-urlencoded",
		  			 headers: { 'x-spark-auth-token': token, 'x-spark-username' : user },
		  			  data: { customerId:customerId, amount: amount, date : new Date(date)},
		  			  success: function(data){
		  				displayBillGeneration();

		  			              },
		  			  error: function(data){
		  				  alert("An error occured while making the payment. Please try again");
		  			  }
		  			});
		  			 
		  		}
		  	 });
		  	             	  
		
 $('#makePayment').click(function(){
	
	var payment=$('#payment').val();
	var customerId = $('#customerId').val();
	if(payment != undefined && payment != ''){
	 $.ajax({		  
		  url: "/spark-cxf/rest/payment/makePayment",
		  type: "POST",
		 headers: { 'Authorization': token, 'spark-user' : user },
		  data: { payment: payment, customerId : customerId	},
		  success: function(data){
			  $("#sbalance").val(data);
			  $("#payment").val('');
		              },
		  error: function(data){
			  alert("An error occured while making the payment. Please try again");
		  }
		});
	 displayPaymentData($('#customerId').val());
		 
	}
 });
             	  
  
  $('#addcustomer').click(function(){
	  $(this).after('</br><img src = "img/load.gif" id="loading" height = "20px" alt ="loading"/>');
	  var name = $('#name').val();
	  var phone = $('#phone').val();
	  var add1 =$('#address1').val();
	  var add2 =$('#address2').val();
	  var city = $('#city').val();
	  var area = $('#area').val();
	  var active = $('#active').val();
	  var balance = $('#balance').val();
	  
	  $.ajax({
		  
		  url: "/spark-cxf/rest/customer/addCustomer",
		  type: "POST",
		 headers: { 'Authorization': token, 'spark-user' : user },
		  data: JSON.stringify({ customerName: name, customerPhone: phone, balance:balance, addressLine1 : add1, addressLine2 : add2, city:city, area:area, active:active}),
		  contentType: 'application/json; charset=UTF-8',
		  success: function(data){
			  $("#loading").remove();
		              },
		  error: function(data){
			  $("#loading").remove();
			  alert("An error occured while creating the new customer. Please check all the fields and try again");
		  }
		});
	  }
	);
  
  $('#save').click(function(){
	  $(this).after('</br><img src = "img/load.gif" id="loading" height = "20px" alt ="loading"/>');
	  var name = $('#tags').val();
	  var phone = $('#tagsphone').val();
	  var add1 =$('#saddress1').val();
	  var add2 =$('#saddress2').val();
	  var city = $('#scity').val();
	  var area = $('#sarea').val();
	  var active = $('#sactive').val();
	  var balance = $('#sbalance').val();
	  var customerId = $("#customerId").val();
	  
	  $.ajax({
		  
		  url: "/spark-cxf/rest/customer/saveCustomer",
		  type: "POST",
		 headers: { 'Authorization': token, 'spark-user' : user },
		  data: JSON.stringify({ customerId:customerId, customerName: name, customerPhone: phone, balance:balance, addressLine1 : add1, addressLine2 : add2, city:city, area:area, active:active}),
		  contentType: 'application/json; charset=UTF-8',
		  success: function(data){
			  $("#loading").remove();
		              },
		  error: function(data){
			  $("#loading").remove();
			  alert("An error occured while creating the new customer. Please check all the fields and try again");
		  }
		});
	  }
	);

  
  $('#searchCustomer').click(function(){
	  $('#searchResult').html("");
	  var name = $('#tags').val();
	  var phone = $('#tagsPhone').val();
	  if(name!='')
	  	{    $(this).after('<img src = "img/load.gif" id="loading" height = "20px" alt ="loading"/>');
			  $.ajax({
				  url: "/spark-cxf/rest/customer/searchCustomerName",
				  type: "GET",
				 headers: { 'Authorization': token, 'spark-user' : user },
				  datatype:"json",
				  contentType:"application/x-www-form-urlencoded",
					 	  accepts: {
				        text: "application/json"
				    },
			  	  data: { name: name},
				  success: function(data){
					displayData(data);
					displayPaymentData($('#customerId').val());
					displayBillGeneration();
					 	},
					error:function(){
						 $("#loading").remove();
  					  $("#accordion").hide();
  					  alert("No such record found");
					}
			  });
	  	}
	  else if (phone!=''){
		  
		  $(this).after('<img src = "img/load.gif" id="loading" height = "20px" alt ="loading"/>');
		  $.ajax({
			  url: "/spark-cxf/rest/customer/searchCustomerPhone",
			  type: "GET",
			  datatype:"json",
			 headers: { 'Authorization': token, 'spark-user' : user },
			  contentType:"application/x-www-form-urlencoded",
			  accepts: {
			        text: "application/json"
			    },
		  	  data: { phone: phone},
			  success: function(data){
				  displayData(data);
				  displayPaymentData($('#customerId').val());
				  displayBillGeneration();
				   }
		  });
  	  }
	  
	  });
  
  
 function displayPaymentData(customerId){
	 $("#paymenttable tr").remove();
	 
	  $.ajax({
		  url: "/spark-cxf/rest/payment/getPaymentDetails",
		  type: "GET",
		 headers: { 'Authorization': token, 'spark-user' : user },
		  datatype:"json",
		  contentType:"application/x-www-form-urlencoded",
		  accepts: {
		        text: "application/json"
		    },
	  	  data:  { customerId: customerId},
		  success: function(data){
			  if(data){
              var len = data.length;
              var txt = "";
              if(len > 0){
            	  txt += "<thead><th>"+"Date" + "</th><th>" + "Customer Name"+"</th><th>" + "Payment" +"</th></thead>";
                  
                  for(var i=0;i<len;i++){
                      if(data[i].cust.customerName && data[i].payment){
                    	    
                          txt += "<tr><td>"+$.datepicker.formatDate('MM dd, yy', new Date(data[i].date))+ "<td>"+data[i].cust.customerName+"</td><td>"+data[i].payment+"</td></tr>";
                      }
                  }
                  if(txt != ""){
                      $("#paymenttable").append(txt);
                      $("#paymenttable").dataTable({});
                      
                  }
              }
          }
			   }
	  });
	 
  }
  
 function displayBillGenerationData(){
	// var dt = $('#billGeneration').dataTable().fnDestroy();
	 $("#billGeneration tr").remove();
	  $.ajax({
		  url: "/spark-cxf/rest/bill/displayBillGeneration",
		  type: "GET",
		 headers: { 'Authorization': token, 'spark-user' : user },
		  datatype:"json",
		  contentType:"application/x-www-form-urlencoded",
		  accepts: {
		        text: "application/json"
		    },
		  success: function(data){
			  if(data){
              var len = data.length;
              var txt = "";
              if(len > 0){
            	  txt += "<thead><th>"+"Date" +"</th><th>" + "Amount" +"</th></thead>";
                  for(var i=0;i<len;i++){
                      if(data[i].amount && data[i].date){
                    	  txt += "<tr><td>"+$.datepicker.formatDate('MM dd, yy', new Date(data[i].date))+ "</td><td>"+data[i].amount+"</td></tr>";
                      }
                  }
                  if(txt != ""){
                    $("#billGeneration").append(txt);
                  }
              }$("#billGeneration").dataTable({
            	  "bDestroy": true
              });
          }
			   }
	  });
	 
  }
  

 function displayData(data)
  {
	  $("#loading").remove();
	  $("#accordion").show();
	
	    for (var i = 0; i < data.length; i++) {
	        
	        $("#tags").val(data[i].customerName);
	        $("#tagsPhone").val(data[i].customerPhone);
	        $("#saddress1").val(data[i].addressLine1);
	        $("#saddress2").val(data[i].addressLine2);
	        $("#customerId").val(data[i].customerId);
	        $("#sarea").val(data[i].area);
	        $("#scity").val(data[i].city);
	        $("#sbalance").val(data[i].balance);
	        $("#sactive option[value=" + data[i].active +"]").attr("selected","selected") ;
	        
	       }
	    }
  
  $(function() {
	  $("#tags").autocomplete({
	    source: function( request, response ) {
	      $.ajax({
	        url: "/spark-cxf/rest/customer/searchCustomer",
	        headers: { 'Authorization': token, 'spark-user' : user },
	        dataType: "json",
	        contentType:"application/x-www-form-urlencoded",
	        accepts: {
		        text: "application/json"
		    },
	  	  
	        type:"GET",
	        data: {
	          name: request.term
	        },
	         success: function( data ) {
	           response( $.map( data, function( item, valu ) {
	             return {
	               label: item.customerName,
	               value: item.customerName
	           }
	         }));
	         }
	      });
	    },
	    minLength: 1
	  });
  });
	 
  
  $(function() {
	  $("#tagsPhone").autocomplete({
	    source: function( request, response ) {
	      $.ajax({
	        url: "/spark-cxf/rest/customer/searchCustomerPhoneAutoComplete",
	       headers: { 'Authorization': token, 'spark-user' : user },
	        dataType: "json",
	        contentType:"application/x-www-form-urlencoded",
	        accepts: {
		        text: "application/json"
		    },
	  	  
	        type:"GET",
	        data: {
	          phone: request.term
	        },
	         success: function( data ) {
	           response( $.map( data, function( item, valu ) {
	             return {
	               label: item.customerPhone,
	               value: item.customerPhone
	           }
	         }));
	         }
	      });
	    },
	    minLength: 1
	  });
  });
	
    
  
});	