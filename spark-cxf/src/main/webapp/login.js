function authenticate(userId,password,sparkToken){
		 $.ajax({		  
			 //create a new service - authentication service and call it here.
				  url: "/spark-cxf/rest/authenticate/validateLogin",
				  type: "POST",
				  contentType:"application/x-www-form-urlencoded",
				  data: { userId: userId, password : password, sparkToken : sparkToken},
				  success: function(data){
					  if(data!="login_fail"){
						  setCookies(data);
						  if(!(window.location.href.indexOf("home")>=0)){
							  window.location.href = "home.jsp";
						  }
					  }
					  else if (data=="login_fail"){
						  window.location.href = "index.jsp";
					  }
					  alert(data);
				  },
				  error: function(data){
					  alert("An error occured while authenticating you. Please try again");
				  }
		});
	}
function setCookies(cookie){
	$.cookie("sparkUser", $('#userId').val(),{
		path:'/'
	});

	$.cookie("sparkToken",cookie ,{
		path:'/'
	});
}



$(document).ready(function(){
	
	$('#submit').click(function(){

		var userId = $('#userId').val();
		var password = $('#password').val();
		var sparkToken = $.cookie("sparkToken");
		authenticate(userId,password,sparkToken);
	});

	
		
});