
<html>
<head>
 <meta charset="utf-8">
 
  <title>Spark Cables</title>
  
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/28e7751dbec/integration/jqueryui/dataTables.jqueryui.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css">
 
<script type ="text/javascript" src="jquery.js"></script>

<script type ="text/javascript" src ="jquery.cookie.js"></script>
<script type ="text/javascript" src="jquery-ui-1.10.4.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/1.10-dev/js/jquery.dataTables.min.js"></script>
<script type ="text/javascript" src="login.js"></script>
<script type ="text/javascript" src="ext.js"></script>

</head>

<body>   
<h2>Spark Cables</h2>

<div id = "main1">

<ul>
<li><a href ="#tab1">  Add Customer</a></li>
<li><a href ="#tab2">  View/Edit Customer</a></li>
<li><a href ="#tab4">  Generate Bills</a></li>
</ul>

<div id = "tab1">
Name : <input type = "text" id = "name"></input>
  Phone : <input type = "text" id = "phone"></input>
<br><br>
Customer Address ::
<br><br>
Address Line 1 : <input type = "text" id = "address1"></input>
<br>Address Line 2 : <input type = "text" id = "address2"></input>
<br>Area  - - - - - : <input type = "text" id = "area"></input>
<br>Town/City      : <input type = "text" id = "city"></input>
<br> Opening Balance : <input type="number" id ="balance"></input>

<br> Active : <select  id= "active">
	<option value=true > Yes</option>
	<option value=false > No</option>
</select>
<br><br>
<input type="button" id = "addcustomer" value= "Register"></input>
</div>

<div id = "tab2">

 <div class="ui-widget">
  <label for="tags">Name: </label>
  <input id="tags">
 
  <label for="tags">Phone: </label>
  <input id="tagsPhone">
  
<input type="button" id = "searchCustomer" value= "Search"></input>
</div>
<div id="accordion" style="display: none;">
	<h3> <a href="#">Customer Details</a></h3>
		<div id="serResult" >

	<input type = "text" id = "customerId" style="display:none"></input>
		
		Customer Address ::
		<br><br>
		Address Line 1 : <input type = "text" id = "saddress1"></input>
		<br>Address Line 2 : <input type = "text" id = "saddress2"></input>
		<br>Area  - - - - - : <input type = "text" id = "sarea"></input>
		<br>Town/City      : <input type = "text" id = "scity"></input>
		<br>  Balance : <input type="number" id ="sbalance"></input>
		
		<br> Active : <select  id= "sactive">
			<option value=true > Yes</option>
			<option value=false > No</option>
		</select>
		<br>
		  Payment : <input type="number" id ="payment"></input>
		  <input type="button" id="makePayment" value="Make Payment"></input>
		<br>
		<br>
		<input type = "button" id="save" value ="Save" style="position: relative; left: 1000px;" ></input>
		<br>
		
		</div>
	

	<h3><a href="#">Payment History</a></h3>
	<div>
	<table id="paymenttable">
	<thead><tr><th>Name</th><th>Position</th><th>Office</th><th>Salary</th></tr>
	</thead>
	
	</table>
	</div>
	
	<h3><a href="#">Bill Generation</a></h3>
	<div>
		Amount : <input type = "number" id = "custbillAmount"></input>
		<p>Date: <input type="text" id="custdatepicker"></p>
 	
		<br><input type = "button" id = "custgenerateBill" value = "Generate Bill"></input>
	 <br>
	 Bill Generation History
	 <table id ="custbillgeneration"><thead><tr><th>Date</th><th>Amount</th></tr>
	</thead>
	</table>
	 
	 	</div>
</div>
</div>

<div id="tab4">
Amount : <input type = "number" id = "billAmount"></input>
<p>Date: <input type="text" id="datepicker"></p>
 

<br><input type = "button" id = "generateBill" value = "Generate Bill"></input>
<br> Bill Generation History <br>
<table id ="billGeneration" class ="display"><thead><tr><th>Date</th><th>Amount</th></tr>
	</thead><tbody></tbody>
		</table>
	
</div>

</div>

 

</body>
</html>
